package com.npe.bot.kafka.config;


import com.npe.bot.kafka.listener.AbstractKafkaListener;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractKafkaConsumerConfig {

    protected abstract String getBootstrapAddress();

    protected abstract String getGroupId();

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                getBootstrapAddress());
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                getGroupId()
        );
        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory(ConsumerFactory<String, String> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaMessageListenerContainer<String, String> kafkaMessageListenerContainer(AbstractKafkaListener kafkaListener,
                                                                                       NewTopic inputTopic) {
        ContainerProperties containerProperties = new ContainerProperties(inputTopic.name());
        containerProperties.setMessageListener(kafkaListener);
        return new KafkaMessageListenerContainer<String, String>(consumerFactory(), containerProperties);
    }
}
