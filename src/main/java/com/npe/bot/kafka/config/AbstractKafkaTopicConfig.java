package com.npe.bot.kafka.config;

import com.npe.dto.kafka.SystemType;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractKafkaTopicConfig {

    protected abstract String getBootstrapAddress();

    protected abstract String getBotsTopic();

    protected abstract SystemType getSystemType();

    protected abstract String getInputTopic();

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> config = new HashMap<>();
        config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapAddress());
        return new KafkaAdmin(config);
    }

    @Bean
    public NewTopic botsTopic() {
        return new NewTopic(getBotsTopic(), 1, (short) 1);
    }

    @Bean
    public NewTopic inputTopic() {
        return new NewTopic(getInputTopic(), 1, (short) 1);
    }

    @Bean
    public SystemType systemType() {
        return getSystemType();
    }
}
