package com.npe.bot.kafka.config;

import com.npe.bot.kafka.listener.AbstractKafkaPayloadTransformer;
import com.npe.dto.kafka.EventType;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AbstractKafkaTransformerConfig {
    private final List<AbstractKafkaPayloadTransformer> transformers;

    public AbstractKafkaTransformerConfig(List<AbstractKafkaPayloadTransformer> transformers) {
        this.transformers = transformers;
    }

    @Bean
    public Map<EventType, AbstractKafkaPayloadTransformer> transformers() {
        return transformers.stream()
                .collect(Collectors.toMap(AbstractKafkaPayloadTransformer::getEventType, transformer -> transformer));
    }
}
