package com.npe.bot.kafka.exception;

public class EmptyPayloadException extends IllegalArgumentException {
    public EmptyPayloadException() {
    }

    public EmptyPayloadException(String s) {
        super(s);
    }

    public EmptyPayloadException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyPayloadException(Throwable cause) {
        super(cause);
    }
}
