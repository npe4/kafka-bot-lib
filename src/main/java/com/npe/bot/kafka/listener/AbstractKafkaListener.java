package com.npe.bot.kafka.listener;

import com.npe.bot.kafka.exception.EmptyPayloadException;
import com.npe.bot.kafka.exception.UserNotFoundException;
import com.npe.bot.kafka.utils.ConversionUtils;
import com.npe.dto.kafka.EventType;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.SystemType;
import io.micrometer.common.util.StringUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;

import java.util.Map;
import java.util.Objects;

@Slf4j
public abstract class AbstractKafkaListener implements AcknowledgingMessageListener<String, String> {

    private final Map<EventType, AbstractKafkaPayloadTransformer> transformerRegistry;

    public AbstractKafkaListener(Map<EventType, AbstractKafkaPayloadTransformer> transformerRegistry) {
        this.transformerRegistry = transformerRegistry;
    }

    @SneakyThrows
    @Override
    public void onMessage(ConsumerRecord<String, String> consumerRecord, Acknowledgment acknowledgment) {

        log.debug("Received " + consumerRecord.value() + " from " + consumerRecord.topic());

        if (StringUtils.isBlank(consumerRecord.value())) {
            throw new EmptyPayloadException();
        }

        MessageWrapper message = ConversionUtils.jsonToJava(consumerRecord.value(), MessageWrapper.class);
        if (!Objects.equals(message.getSystemType(), SystemType.VK)) {
            return;
        }

        if (!userFound(message.getCustomerContact())) {
            throw new UserNotFoundException();
        }

        sendMessage(message.getCustomerContact(), transformerRegistry.get(message.getEventType()), message);

        acknowledgment.acknowledge();
    }

    protected abstract boolean userFound(String customerContact);

    protected abstract void sendMessage(String userTag, AbstractKafkaPayloadTransformer transformer, MessageWrapper message);
}
