package com.npe.bot.kafka.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.npe.bot.kafka.utils.ConversionUtils;
import com.npe.dto.kafka.EventType;
import lombok.Getter;

@Getter
public abstract class AbstractKafkaPayloadTransformer<T> {
    public AbstractKafkaPayloadTransformer(EventType eventType) {
        this.eventType = eventType;
    }

    protected EventType eventType;
    public abstract String transform(T obj);

    public T map(Object value) {
        return ConversionUtils.mapper.convertValue(value, getClazz());
    }

    protected abstract Class<T> getClazz();
}
