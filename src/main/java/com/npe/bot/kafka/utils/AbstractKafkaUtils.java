package com.npe.bot.kafka.utils;

import com.npe.dto.kafka.EventType;
import com.npe.dto.kafka.MessageWrapper;
import com.npe.dto.kafka.SystemType;
import com.npe.dto.kafka.TargetType;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class AbstractKafkaUtils {
    private final NewTopic botsTopic;
    private final KafkaProducer<String, String> kafkaProducer;

    private final SystemType systemType;

    public AbstractKafkaUtils(NewTopic botsTopic, @Lazy KafkaProducer<String, String> kafkaProducer, SystemType systemType) {
        this.botsTopic = botsTopic;
        this.kafkaProducer = kafkaProducer;
        this.systemType = systemType;
    }

    public void send(Object payload, String userTag, EventType eventType) {
        String corrId = UUID.randomUUID().toString();
        kafkaProducer.send(new ProducerRecord<>(
                botsTopic.name(),
                corrId,
                ConversionUtils.javaToJson(new MessageWrapper<>(
                        userTag,
                        systemType,
                        TargetType.BANK,
                        eventType,
                        corrId,
                        Optional.of(payload)
                ))
        ));
    }
}
